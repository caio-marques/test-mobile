package com.br.inmetrics.pages.waycurso;

import static com.br.inmetrics.frm.helpers.QueryHelper.*;

import com.br.inmetrics.frm.base.PageBase;
import com.br.inmetrics.frm.base.VirtualElement;

public class LoginPage extends PageBase {

	VirtualElement
    			   btnPermitir = getElementById("br.com.santander.ewallet.ui.activities.SplashActivity"),
    			   inputCpf = getElementById("br.com.santander.way:id/edtUser"),
		           inputSenha = getElementById("br.com.santander.way:id/edtPassword"),
		           alertAtencao = getElementById("br.com.santander.way:id/title"),
		           bntAlertOk = getElementById("br.com.santander.way:id/btOkResetOff"),
		           btnEntrarLogin = getElementById("br.com.santander.way:id/btnEnter"),
		           btnAceitarTermos = getElementById("br.com.santander.way:id/btnAccept"),
		           btnNaoAceitarTemos = getElementById("br.com.santander.way:id/btnNotAccept"),
		           txtDigital = getElementById("br.com.santander.way:id/tvOfferTitle"),
		           btnContinuar = getElementById("br.com.santander.way:id/btContinue"),
		           btnFecharSlides = getElementById("br.com.santander.way:id/close"),
		           txtFaturaAbertaInicio = getElementById("");
		           
	
}
