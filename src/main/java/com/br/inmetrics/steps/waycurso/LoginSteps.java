package com.br.inmetrics.steps.waycurso;

import com.br.inmetrics.frm.bdd.Step;
import com.br.inmetrics.pages.waycurso.LoginPage;

public class LoginSteps {
	
	LoginPage loginPage = new LoginPage();
	
	@Step("Dado que o acesso o aplicativo Way")
	public void acesso_ao_app_way() {
		
	}
	
	@Step("Quando eu realizar o login")
	public void quando_realizar_login() {
		
	}
	
	@Step("Então devo visualizar a home com sucesso")
	public void validando_home_sucesso() {
		
	}
	

}
