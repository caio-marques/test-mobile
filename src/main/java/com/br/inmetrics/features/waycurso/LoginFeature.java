package com.br.inmetrics.features.waycurso;

import static com.br.inmetrics.frm.bdd.Gherkin.*;

import java.util.concurrent.ExecutionException;

import com.br.inmetrics.frm.bdd.Feature;
import com.br.inmetrics.frm.bdd.Scenario;

@Feature("Login")
public class LoginFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Valor Login Correntista")
	public void validarLoginCorrentista() throws ExecutionException {
		given_("Dado que o acesso o aplicativo Way").
		when_("Quando eu realizar o login").
		then_("Então devo visualizar a home com sucesso").
		execute_();
	}

}
